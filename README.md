# Baseline AKS Cluster

[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/ralgar/aks-baseline?branch=main&label=Pipeline&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/aks-baseline/-/pipelines?page=1&scope=all&ref=main)
[![Open Issues](https://img.shields.io/gitlab/issues/open/ralgar/aks-baseline?color=blue&style=for-the-badge)](https://gitlab.com/ralgar/aks-baseline/-/issues)
[![Open Merge Requests](https://img.shields.io/gitlab/merge-requests/open/ralgar/aks-baseline?color=purple&style=for-the-badge)](https://gitlab.com/ralgar/aks-baseline/-/merge_requests)
[![Software License](https://img.shields.io/gitlab/license/ralgar/aks-baseline?color=orange&style=for-the-badge)](https://mit-license.org)
[![GitLab Stars](https://img.shields.io/gitlab/stars/ralgar/homelab?color=gold&label=Stars&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/homelab)

## Overview

This project follows the GitOps paradigm to provision an AKS Cluster.

**Note:** This is still a WIP

### Features

- [x] Automated provisioning and deployment, using GitLab CI and Flux CD
- [x] Self-updating, using Azure functionality and [Kured](https://kured.dev).
- [x] Prometheus + Grafana for observability.
- [x] Integrated with Azure Key Vault via Azure AD Workload Identity.

## Initial Setup

The setup process for this project is fairly simple, but requires you to be
 diligent. Be sure to follow each step carefully and completely, or it will
 not work as expected.

### Prerequisites

You will need to install these on your local system:

- azure-cli
- kubectl

You will also need a Microsoft Azure account, and an active subscription.

### Configuring Microsoft Azure

1. Login to the Azure CLI, and change your current subscription if needed.

   ```sh
   az login

   az account set --subscription <my_subscription>
   ```

1. Enable the [Workload Identity Preview](https://learn.microsoft.com/en-us/azure/aks/workload-identity-deploy-cluster#register-the-enableworkloadidentitypreview-feature-flag)
   feature for AKS.

   ```sh
   # Enable the feature flag
   az feature register --namespace "Microsoft.ContainerService" --name "EnableWorkloadIdentityPreview"

   # Wait until the status shows "Registered" (will take several minutes)
   az feature show --namespace "Microsoft.ContainerService" --name "EnableWorkloadIdentityPreview"

   # Refresh the registration of the Microsoft.ContainerService provider
   az provider register --namespace Microsoft.ContainerService
   ```

1. Create an Azure AD Service Principal Application. Be sure to keep the output
   values from this command handy, you'll need them in the next steps.

   ```sh
   export MSYS_NO_PATHCONV=1
   az ad sp create-for-rbac \
       --name Terraform \
       --role Owner \
       --scopes /subscriptions/<subscription_id>
   ```

1. Grant the required permissions for the Microsoft Graph API, using the
   **Application ID** output from the previous step.

   ```sh
   az ad app permission admin-consent \
       --id <service_principal_application_id>

   az ad app permission add \
       --id <service_principal_application_id> \
       --api 00000003-0000-0000-c000-000000000000 \
       --api-permissions 1bfefb4e-e0b5-418b-a88f-73c46d2cc8e9=Role

   az ad app permission grant \
       --id <service_principal_application_id> \
       --api 00000003-0000-0000-c000-000000000000 \
       --scope Application.ReadWrite.All
   ```

### Configure your GitLab repository

1. Fork this project.

1. In your project, go to **Settings -> CI/CD -> Variables**, and create the
   following variables. Be sure to set them as *non-protected* and *masked*.

   ```sh
   ARM_SUBSCRIPTION_ID="<azure_subscription_id>"
   ARM_TENANT_ID="<azure_subscription_tenant_id>"
   ARM_CLIENT_ID="<service_principal_appid>"
   ARM_CLIENT_SECRET="<service_principal_password>"
   ```

## Usage

Once you have completed the [Initial Setup](#initial-setup), you can go ahead
 and deploy the cluster.

### Deploying the cluster

1. In your GitLab project, go to **CI/CD -> Pipelines -> Run pipeline**.

1. Make sure the **Branch** is set to *main*, and choose **Run pipeline**.

The CI/CD pipeline will automatically deploy your new AKS cluster, and a
 kube config file will be output as an artifact.

### Interacting with the cluster using kubectl

1. Download the `kube_config` artifact from your initial pipeline run.

1. Extract the kube config file to `~/.kube/config`, or place it anywhere else
   you like and set the `KUBECONFIG` environment variable accordingly.

1. Use the `kubectl` command to interact with your cluster.

### Making modifications to the cluster / infrastructure

1. Clone your repository.

   ```sh
   git clone git@gitlab.com:<your_namespace>/<your_repository>
   ```

1. Make your modifications, and push the changes back to GitLab.

   ```sh
   git push -u origin main
   ```

The CI/CD pipeline will run automatically, updating the Azure infrastructure
 and/or Kubernetes deployments.

### Destroying the infrastructure

1. In your GitLab project, go to **Deployments -> Environments**.

1. On the `production` environment, choose **Stop**, and follow the prompt.

## License

Copyright (c) 2022, Ryan Algar
 ([ralgar/aks-baseline](https://gitlab.com/ralgar/aks-baseline))

MIT License (see LICENSE or [MIT License](https://mit-license.org))
