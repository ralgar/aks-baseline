variable "environment" {
  description = "Deployment environment (prod, staging, demo)."
  type        = string
}

variable "location" {
  description = "Location (region) for resources."
  default     = "West US 3"
  type        = string
}

variable "prefix" {
  description = "Prefix for naming resources."
  type        = string
}
